#include <iostream>
#include <cstdint>
#include <limits>
using namespace std;

int i = numeric_limits<int>::max();
long l = numeric_limits<long>::max();
int64_t i64 = numeric_limits<int64_t>::max();;
int32_t i32t = numeric_limits<int32_t>::max();
uint64_t ui64t = numeric_limits<uint64_t>::max();
long double ld = numeric_limits<long double>::max();

void integer(){
  size_t a = sizeof(i);
  cout << "int      max:               " << i << " size: " << a << " bytes" << endl;
}

void sizeOfint32_t(){
  size_t a = sizeof(i32t);
  cout << "int32_t  max:               " << i32t << " size: " << a << " bytes" << endl;
}

void longe(){
  cout << "long     max:      " << l << " size: " << sizeof(l) << " bytes" << endl;
}

void int64(){
  size_t a = sizeof(i64);
  cout << "int64    max:      " << i64 << " size: " << a << " bytes" << endl;
}

void sizeOfuint64_t(){
  size_t a = sizeof(ui64t);
  cout << "uint64_t max:     " << ui64t << " size: " << a << " bytes" << endl;
}

void longDouble() {
  size_t a = sizeof(long double);
  cout << "long double max:         " << ld << " size: " << a << " bytes" << endl;
}

int main(){
  integer();
  sizeOfint32_t();
  longe();
  int64();
  sizeOfuint64_t();
  longDouble();
  return 0;
}
/*
1. This file (add.cpp) is the implementation of the header's (add.h) stub (interface)
2. Compile add.cpp first, before using the header (add.h)
3. g++ -c add.cpp
4. specifying the -c switch (compile only, 'not a program with main()') creates an 'add.o' object file, 
   ready for linking with another program 
*/
#include "add.h"
int add1(int n) {
  return n + 1;
}
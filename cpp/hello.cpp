#include <iostream>
#include <cassert>
/*in order to include the add.h (header) implementation,
  1. see the instructions in add.cpp
  2. then link and compile this program along with the add.o file,
     which is the result of the above compile step (see 1.):
  3. g++ hello.cpp add.o -o hello
  4. another option is to simply reference the add.cpp file, which references add.h:
  5. g++ hello.cpp add.cpp -o hello
*/
#include "add.h"
#include "libs/linkme-static.h"
#include "libs/isPalindrome.h"
using namespace std;

#define MUL(a,b) a*b

int main(){
  //int x = 1;
  //int y = add1(x);
  //printf("y = {0}", y);

  cout << "2+3=" << add(2,3) << endl;

  while(1) {
    char buffer[64] = {0};
    cin >> buffer;

    if(isPalindrome(buffer)) {
      cout << buffer << " is a palindrome" << endl; 
    } else {
      cout << buffer << " is NOT a palindrome" << endl;
    }
  }

  assert(MUL(2,4) == 8);
  #if _DEBUG
    printf("DEBUG run\n");
    //g++ -D DEBUG hello.cpp -o hello
  #else 
    printf("Release run\n"); 
    //g++ hello.cpp -o hello
  #endif

  //cout << "Hello C++" << endl;
  return 0;
}
#if defined _MSC_VER || _WIN32 || __CYGWIN__
  // MSFT:
  #define EXPORT __declspec(dllexport)
  #define IMPORT __declspec(dllimport)
#elif defined __GNUC__ 
  // GCC:
  #define EXPORT __attribute__((visibility("default")))
  #define IMPORT
#else
  //fuzzy logic?
  #define EXPORT
  #define IMPORT
  #pragma warning Unknown dynamic link import/export semantics.
#endif

extern "C" EXPORT bool isPalindrome(char* word);

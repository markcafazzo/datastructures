#include <stdio.h>
#include "./chalk.h"
//POINTERS - variables that store address of another variable

/*    MEMORY
   -----------
208| b = 10  | <- then, point P to b
204|---------|
   | a = 4   | <- first, point P to a
   |---------|
   |         |
   |---------|
 64| P = 208 | -> P (pointer)
   |---------|
   |         |
   -----------
*/

void printMemoryMap(){
  printf("   -----------\n");
  printf("208| b = 10  | <- then, point P to b\n");
  printf("204|---------|\n");
  printf("   | a = 4   | <- first, point P to a\n");
  printf("   |---------|\n");
  printf("   |         |\n");
  printf("   |---------|\n");
  printf(" 64| P = 208 | -> P (pointer)\n");
  printf("   |---------|\n");
  printf("   |         |\n");
  printf("   -----------\n");
}

void pointerIntro() {
  printMemoryMap();
  int a = 2; //at address '204' (4 bytes)
  int b = 10; //at address '208' (4 bytes)
  int *p; //pointer variable...
  p = &a; //now points to var 'a' at address 204
  printf("Address of a: " GREEN "%p" RESET "\n", &a);
  printf("Value of a: %i\n", a);
  printf("Size of a: %ld\n", sizeof(a));
  printf("Address of p itself: " GREEN "%p\n" RESET, &p);
  printf("Address of p (pointing to a):" GREEN "%p\n" RESET, p);
  printf("Value of p (pointing to a): %i\n", *p);
  printf("Incrementing variable a...\n");
  a++;
  printf("Value of p (pointing to a): %i\n", *p);
  printf("----------------------------\n");
  printf("Address of b: " GREEN "%p\n" RESET, &b);
  p = &b;//now points to var 'b' at address 208
  printf("Address of p (pointing to b): " GREEN "%p\n" RESET, p);
  printf("Value of p (dereferencing b): %i\n", *p);
  printf("Incrementing variable b...\n");
  b++;
  printf("Value of p (dereferencing b): %i\n", *p);
  *p = 200;
  printf("Value of b (updating b via pointer): %i\n", *p);
  printf("Value of b (direct): %i\n", b);
  printf("----------------------------\n");
  printf("Pointing p back to a...\n");
  p = &a;
  printf("Value of p (dereferencing from address of a): %i\n", *p);
  p = p+1;
  printf("Value of p (dereferencing, after +1): %i\n", *p);
  printf("Address of p (pointing to b, after +1): " GREEN "%p\n" RESET, p);
  // P -> address
  //*P -> value at address
}

int main(int argc, char* argv[]){
  pointerIntro();
}

//gcc -o pointer pointer.c

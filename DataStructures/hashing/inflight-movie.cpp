#include <iostream>
#include <vector>
#include <unordered_set>
#include <chrono>

using namespace std;
using Clock=chrono::high_resolution_clock;

bool canTwoMoviesFillFlight(const vector<int>& movieLengths, int flightLength)
{
    auto start = Clock::now();

    //Interview Cake example - use unordered_set, find and insert within loop (really, really slow):
    unordered_set<int> movieLengthsSeen;
    for(int firstMovieLength : movieLengths) {
      int matchingSecondMovieLength = flightLength - firstMovieLength;
      if(movieLengthsSeen.find(matchingSecondMovieLength) != movieLengthsSeen.end()) {
        auto end = Clock::now();
        cout << "time: " << chrono::duration_cast<chrono::nanoseconds>(end - start).count() << " | ";
        return true;
      }

      movieLengthsSeen.insert(firstMovieLength);
    }

    // determine if two movies add up to the flight length
    // if(movieLengths.size() == 0) {
    //   return false;
    // }

    // solution filling a hashtable (unordered_map) with entire movieLengths contents - way slower:
    // unordered_map<int, int> movies;
    // for(int ii = 1; ii < movieLengths.size(); ++ii) {
    //   movies.insert(make_pair(movieLengths[ii], movieLengths[ii]));
    // }
    
    // for(int i = 0; i < movieLengths.size(); ++i) {
    //   int firstMovieTime = movieLengths[i];
    //   int remainder = flightLength - firstMovieTime;
    //   if(movies.count(remainder) > 0) {
    //     auto end = Clock::now();
    //     cout << "time: " << chrono::duration_cast<chrono::nanoseconds>(end - start).count() << " | ";
    //     return true;
    //   }
    // }

    // secondary solution using nested loop - way faster:
    // for(int i = 0; i < movieLengths.size(); ++i) {
    //   int firstMovieTime = movieLengths[i];
    //   for(int j = 1; j < movieLengths.size(); ++j) {
    //     int remainder = flightLength - firstMovieTime;
    //     if(remainder == movieLengths[j])
    //     {
    //       auto end = Clock::now();
    //       cout << "time: " << chrono::duration_cast<chrono::nanoseconds>(end - start).count() << " | ";
    //       return true;
    //     }
    //   }
    // }
    auto end = Clock::now();
    cout << "time: " << chrono::duration_cast<chrono::nanoseconds>(end - start).count() << " | ";
    return false;
}
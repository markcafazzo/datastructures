#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

int main () {
  unordered_map<string, int> lightBulbHoursOfLight;

  lightBulbHoursOfLight.insert(make_pair("incandescent", 1200));
  lightBulbHoursOfLight.insert(make_pair("compact flourescent", 10000));
  lightBulbHoursOfLight.insert(make_pair("LED", 50000));
  

  return 0;
}
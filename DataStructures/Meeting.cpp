#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

class Meeting {
  private:
    // number of 30 min blocks past 9:00 am
    unsigned int startTime_;
    unsigned int endTime_;
  public:
    Meeting() :
        startTime_(0),
        endTime_(0)
    {}

    Meeting(unsigned int startTime, unsigned int endTime) :
        startTime_(startTime),
        endTime_(endTime)
    {}

    string toString() const {
      string outp;
      outp.append("{" + to_string(getStartTime()) + ", " + to_string(getEndTime()) + "}");
      return outp;
    }

    unsigned int getStartTime() const
    {
        return startTime_;
    }

    void setStartTime(unsigned int startTime)
    {
        startTime_ = startTime;
    }

    unsigned int getEndTime() const
    {
        return endTime_;
    }

    void setEndTime(unsigned int endTime)
    {
        endTime_ = endTime;
    }

    bool operator==(const Meeting& other) const
    {
        return
            this->startTime_ == other.startTime_
            && this->endTime_ == other.endTime_;
    }

    bool operator<(const Meeting& other) const 
    {
      return this->getStartTime() < other.getStartTime();
    }
};

void printout(const vector<Meeting>& meetings){
  unsigned int counter = 0;

  string outp;
  outp.append("Merging meetings... [");
  //cout << "Merging meetings... [";
  for (auto &mtg : meetings) {
    counter++;
    outp.append("{" + to_string(mtg.getStartTime()) + "," + to_string(mtg.getEndTime()) + "}");
    if(counter == meetings.size()) {
      outp.append("]");
    } else {
      outp.append(",");
    }
  }
  cout << outp << endl;
}

bool compareMeetingsByStartTime(const Meeting& firstMeeting, const Meeting& secondMeeting) {
    return firstMeeting.getStartTime() < secondMeeting.getStartTime();
}

//auto x - x is a copy of each vector item
//auto &x - x is a reference to each vector item. change vec's items by changing x 
//auto const &x - work with original items, cannot modify them.
vector<Meeting> mergeRanges(vector<Meeting> &meetings) { //printout(meetings)
  //merge meeting ranges:
  int counter = 0;
  //sort by StartTime:
  std::sort(meetings.begin(), meetings.end(), compareMeetingsByStartTime);
  for (auto &mtg : meetings) 
  {
    int idx = counter + 1;
    if(idx == meetings.size()) break;
    //discover if next meeting overlaps:
    if(mtg.getEndTime() >= meetings[idx].getStartTime())
    {
      if(mtg.getEndTime() < meetings[idx].getEndTime()) {
        mtg.setEndTime(meetings[idx].getEndTime());
      }
      meetings.erase(meetings.begin() + idx);
      mergeRanges(meetings);
      break;
    }
    counter++;
  }
  return meetings;
}

vector<Meeting> mergeRanges(const vector<Meeting> &meetings) {
  vector<Meeting> sorted(meetings);
  sort(sorted.begin(), sorted.end(), compareMeetingsByStartTime);

  vector<Meeting> merged;
  //initialise with 1st meeting:
  merged.push_back(sorted.front());

  for (const Meeting& current : sorted) {
    //get latest merged meeting:
    Meeting &lastMergedMeeting = merged.back();
    if(current.getStartTime() <= lastMergedMeeting.getEndTime()) {
      lastMergedMeeting.setEndTime(max(lastMergedMeeting.getEndTime(), current.getEndTime()));
    } else {
      //add current as-is, no overlap:
      merged.push_back(current);
    }
  }
  return merged;
}

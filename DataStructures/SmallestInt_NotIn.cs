using System.Linq;

namespace DataStructures {

  public class Arrays {
    public static int SmallestInteger(int[] A)
    { 
      int size = A.Max();
      if(size < 0) {
        size = A.Min() * -1;
      }
      int[] indexPosition = new int[size + 1];
      for(int i = 0; i < A.Length; i++) 
      {
        int position = A[i];
        if(position < 0) {
          position = position * -1;
        }
        if(position > size) { break; }
        indexPosition[position] = A[i];
      }
      
      for(int i = 1; i < indexPosition.Length; i++) 
      {
        if(indexPosition[i] != i) {
          return i;
        }
      }
      return size + 1;
    }
  }
}
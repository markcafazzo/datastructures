#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iterator>
using namespace std;

void reverseWords(string& message) {
  istringstream stream(message);
  vector<string> words{istream_iterator<string>{stream},
    istream_iterator<string>{}};
  message = "";
  for(vector<string>::reverse_iterator it = words.rbegin(); it != words.rend(); ++it) {
    message.append(*it + " ");
  }
  message = message.substr(0, message.length() - 1);
}
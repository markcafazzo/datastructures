#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iterator>
using namespace std;

void reverseLetters(string &str) {
  int leftIndex = 0;
  int rightIndex = str.length() -1 ;

  while(leftIndex < rightIndex) {
    char temp = str[leftIndex];
    str[leftIndex] = str[rightIndex];
    str[rightIndex] = temp;

    //move toward the middle:
    leftIndex++;
    rightIndex--;
  }
}

void reverseWords(string& message) {
  istringstream stream(message);
  vector<string> words{istream_iterator<string>{stream},
    istream_iterator<string>{}};
  message = "";
  for(vector<string>::reverse_iterator it = words.rbegin(); it != words.rend(); ++it) {
    message.append(*it + " ");
  }
  message = message.substr(0, message.length() - 1);
}

void reverseCharacters(string &str, size_t leftIndex, size_t rightIndex) {
  while (leftIndex < rightIndex) {
    swap(str[leftIndex], str[rightIndex]);
    ++leftIndex;
    --rightIndex;
  }
}

void reverseWords2(string& message) {
  if(message.length() == 0) return;
  //reverse entire message to get right word order, but reversed:
  reverseCharacters(message, 0, message.length() - 1);

  //make words forward again, char by char:
  //store the *start* index while looking for the *end*:
  size_t currentWordStartIndex = 0;
  for(size_t i = 0; i <= message.length(); ++i) {
    if(i == message.length() || message[i] == ' ') {
      reverseCharacters(message, currentWordStartIndex, i - 1);
      currentWordStartIndex = i + 1;
    }
  }

}





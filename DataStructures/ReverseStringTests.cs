using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace DataStructures {
  public class ReverseStringTests
  {
    [Fact]
    public void EmptyStringTest()
    {
        var expected = "".ToCharArray();
        var actual = "".ToCharArray();
        StringUtils.Reverse(actual);
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void SingleCharacterStringTest()
    {
        var expected = "A".ToCharArray();
        var actual = "A".ToCharArray();
        StringUtils.Reverse(actual);
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void LongerStringTest()
    {
        var expected = "EDCBA".ToCharArray();
        var actual = "ABCDE".ToCharArray();
        StringUtils.Reverse(actual);
        Assert.Equal(expected, actual);
    }
  }
}
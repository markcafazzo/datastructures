using System;

namespace DataStructures 
{
  public static class StringUtils 
  {
    public static void Reverse(char[] arrayOfChars)
    {
      char[] tempArray = new char[arrayOfChars.Length];
      arrayOfChars.CopyTo(tempArray, 0);
      for(int i = 0; i < arrayOfChars.Length; i++)
      {
        char temp = tempArray[(tempArray.Length - 1) - i];
        arrayOfChars[(arrayOfChars.Length - 1) - i] = tempArray[i];
        arrayOfChars[i] = temp;
      }
    }

    public static void Reverse2(char[] arrayOfChars)
    {
      int leftIndex = 0;
      int rightIndex = arrayOfChars.Length - 1;
      while (leftIndex < rightIndex) 
      {
        char temp = arrayOfChars[leftIndex];
        arrayOfChars[leftIndex] = arrayOfChars[rightIndex];
        arrayOfChars[rightIndex] = temp; 
        //move towards the middle:
        leftIndex++;
        rightIndex--; 
      } 
    }
  }
}
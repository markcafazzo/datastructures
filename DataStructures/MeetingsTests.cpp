#include <iostream>
#include <algorithm>
#include <vector>
#include "Meeting.cpp"
#include "./libs/lest.hpp"

using namespace std;

// tests
const lest::test tests[] = {
    CASE("meetings overlap") {
        const auto meetings = 
        vector<Meeting> {Meeting(1, 3), Meeting(2, 4)};
        const auto actual = mergeRanges(meetings);
        const auto expected = vector<Meeting> {Meeting(1, 4)};
        EXPECT(actual == expected);
    },
    CASE("meetings touch") {
        const auto meetings = 
        vector<Meeting> {Meeting(5, 6), Meeting(6, 8)};
        const auto actual = mergeRanges(meetings);
        const auto expected = vector<Meeting> {Meeting(5, 8)};
        EXPECT(actual == expected);
    },
    CASE("meeting contains other meeting") {
        vector<Meeting> meetings = 
        vector<Meeting> {Meeting(1, 8), Meeting(2, 5)};
        const auto actual = mergeRanges(meetings);
        const auto expected = vector<Meeting> {Meeting(1, 8)};
        EXPECT(actual == expected);
    },
    CASE("meetings stay separate") {
        vector<Meeting> meetings = 
        vector<Meeting> {Meeting(1, 3), Meeting(4, 8)};
        const auto actual = mergeRanges(meetings);
        const auto expected = 
        vector<Meeting> {Meeting(1, 3), Meeting(4, 8)};
        EXPECT(actual == expected);
    },
    CASE("multiple merged meetings") {
        vector<Meeting> meetings = 
        vector<Meeting> {Meeting(1, 4), Meeting(2, 5), Meeting(5, 8)};
        const auto actual = mergeRanges(meetings);
        const auto expected = vector<Meeting> {Meeting(1, 8)};
        EXPECT(actual == expected);
    },
    CASE("meetings not sorted") {
        vector<Meeting> meetings = 
        vector<Meeting> {Meeting(5, 8), Meeting(1, 4), Meeting(6, 8)};
        const auto actual = mergeRanges(meetings);
        const auto expected = 
        vector<Meeting> {Meeting(1, 4), Meeting(5, 8)};
        EXPECT(actual == expected);
    },
    CASE("sample input") {
        vector<Meeting> meetings = 
        vector<Meeting> {Meeting(0, 1), Meeting(3, 5), Meeting(4, 8),
            Meeting(10, 12), Meeting(9, 10)};
        const auto actual = mergeRanges(meetings);
        const auto expected = 
        
        
        vector<Meeting> {Meeting(0, 1), Meeting(3, 8), Meeting(9, 12)};
        EXPECT(actual == expected);
    }
};

int main(int argc, char** argv)
{
  return lest::run(tests, argc, argv);
}
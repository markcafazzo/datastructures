using System;
using System.Collections.Generic;
using System.Linq;
using Shouldly;
using Xunit;

namespace DataStructures 
{
  public class Meeting
  {
      public int StartTime { get; set; }

      public int EndTime { get; set; }

      public Meeting(int startTime, int endTime)
      {
          // Number of 30 min blocks past 9:00 am
          StartTime = startTime;
          EndTime = endTime;
      }

      public override bool Equals(object obj)
      {
          if (obj == null || GetType() != obj.GetType())
          {
              return false;
          }

          if (ReferenceEquals(obj, this))
          {
              return true;
          }
          
          var meeting = (Meeting)obj;
          return StartTime == meeting.StartTime && EndTime == meeting.EndTime;
      }

      public override int GetHashCode()
      {
          var result = 17;
          result = result * 31 + StartTime;
          result = result * 31 + EndTime;
          return result;
      }

      public override string ToString()
      {
          return $"({StartTime}, {EndTime})";
      }
  }

  public class MeetingUtils
  {
    /*
    //temp list of items for removal (iterate, remove and continue)
    List<Meeting> toRemove = new List<Meeting>();
    foreach(var meeting in toRemove) { meetings.Remove(meeting); }
    */
    public static List<Meeting> MergeRanges(List<Meeting> meetings) {
      // Merge meeting ranges:
      int counter = 0;
      //sort by StartTime:
      meetings.Sort(delegate(Meeting x, Meeting y) { return x.StartTime.CompareTo(y.StartTime); });

      foreach(var meeting in meetings)
      {
        int idx = counter + 1;
        if(idx == meetings.Count) break;
        
        int nextStartTime = meetings[idx].StartTime;
        int nextEndTime = meetings[idx].EndTime;
        //discover if next meeting overlaps:
        if(meeting.EndTime >= nextStartTime) 
        {
          meeting.EndTime = meeting.EndTime >= nextEndTime 
          ? meeting.EndTime 
          : nextEndTime; 
          meetings.RemoveAt(idx);
          MergeRanges(meetings);
          break;
        }    
        counter++;
      }
      return meetings;
    } 
  }
}

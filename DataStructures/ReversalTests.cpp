#include <iostream>
#include "reverse.cpp"
#include "./libs/lest.hpp"

using namespace std;

const lest::test tests[] = {
  CASE("[words] one") {
        const string expected = "vault";
        string actual = "vault";
        reverseWords2(actual);
        EXPECT(actual == expected);
    },
    CASE("[words] two") {
        const string expected = "cake thief";
        string actual = "thief cake";
        reverseWords2(actual);
        EXPECT(actual == expected);
    },
    CASE("[words] three") {
        const string expected = "get another one";
        string actual = "one another get";
        reverseWords2(actual);
        EXPECT(actual == expected);
    },
    CASE("[words] multiple, same length") {
        const string expected = "the cat ate the rat";
        string actual = "rat the ate cat the";
        reverseWords2(actual);
        EXPECT(actual == expected);
    },
    CASE("[words] multiple, different lengths") {
        const string expected = "chocolate bundt cake is yummy";
        string actual = "yummy is cake bundt chocolate";
        reverseWords2(actual);
        EXPECT(actual == expected);
    },
    CASE("[words] empty string") {
        const string expected;
        string actual;
        reverseWords2(actual);
        EXPECT(actual == expected);
    },
    CASE("[letters] empty string") {
        const string expected;
        string actual;
        reverseLetters(actual);
        EXPECT(actual == expected);
    },
    CASE("[letters] single character string") {
        const string expected("A");
        string actual("A");
        reverseLetters(actual);
        EXPECT(actual == expected);
    },
    CASE("[letters] longer string") {
        const string expected("EDCBA");
        string actual("ABCDE");
        reverseLetters(actual);
        EXPECT(actual == expected);
    }
};

int main(int argc, char** argv)
{
  //lest::run(tests, argc, argv);
  return lest::run(tests, argc, argv);
}
﻿using System;
using System.Collections.Generic;
using Shouldly;

namespace DataStructures
{
  class Program
  {
    static List<Node> _nodes = new List<Node>();

    #region chaining nodes (basic)
    static void PrintList(Node node)
    {
      while(node != null) 
      {
        Console.WriteLine("Node value: {0} next: {1}", node.Value, node.Next != null ? node.Next.Value.ToString() : "");
        node = node.Next;
      }
    }

    static void CreateNodes(int[] nodeValues) 
    {
      foreach(int v in nodeValues) 
      {
        Node node = new Node { Value = v };
        _nodes.Add(node);
      }

      var counter = 1;
      foreach(var n in _nodes)
      {
        if(counter == _nodes.Count) break;
        n.Next = _nodes[counter];
        counter++;
      }

      PrintList(_nodes[0]);
    }
    #endregion chaining nodes (basic)

    #region singly linked list
    #endregion singly linked list

    static void Main(string[] args) 
    {
      MeetingsTests tests = new MeetingsTests();
      tests.MultipleMergedMeetingsTest();
      /*
        //Console.WriteLine("Creating nodes...");
        //int[] vals = new int[] {3,5,7,9,11,13};
        //CreateNodes(vals);

        int smallest = 0;
        int[] array = new int[] {1,3,6,4,1,2};
        smallest = Arrays.SmallestInteger(array);
        Console.WriteLine("Smallest integer = {0}", smallest);
        smallest.ShouldBe(5);

        array = new int[] {-1,-3,-4,-20};
        smallest = Arrays.SmallestInteger(array);
        Console.WriteLine("Smallest integer = {0}", smallest);
        smallest.ShouldBe(5);

        array = new int[] {1,2,3};
        smallest = Arrays.SmallestInteger(array);
        Console.WriteLine("Smallest integer = {0}", smallest);
        smallest.ShouldBe(4);

        array = new int[] {12,9,3,7,5,3,11,4,1};
        smallest = Arrays.SmallestInteger(array);
        Console.WriteLine("Smallest integer = {0}", smallest);
        smallest.ShouldBe(2);

        array = new int[10005];
        for(int i = 0; i < 10005; i++) {
          if (i % 5 == 0) {
            array[i] = i * -1;
          } else if (i == 5) {
            continue;
          } else {
            array[i] = i;
          }
        }

        //array = new int[] {97,99,1,2,3,4,5,6,7,8,9,19,32,57,73,34,110,112, -2};
        smallest = Arrays.SmallestInteger(array);
        Console.WriteLine("Smallest integer = {0}", smallest);
        smallest.ShouldBe(5);
      */
    }
  }
}

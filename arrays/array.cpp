#include <iostream>
#include <array>
#include <string>
#include <cstring>
using namespace std;

//initialize:
std::array<int, 5> gasPrices;
string* cakeFlavours = new string[6];
int sequence[0];

void resize(string *array, int newSize){
  string* newArray = new string[newSize];
  memcpy(newArray, cakeFlavours, (newSize - 1) * newSize * sizeof(array));
  delete [] cakeFlavours;
  array = newArray;
}

int main() {
  //add at indices in order:
  cakeFlavours[0] = "Chocolate";
  cakeFlavours[1] = "Vanilla";
  //"Raspberry" will be INSERTED here
  cakeFlavours[2] = "Red Velvet";
  cakeFlavours[3] = "Carrot";//this will get overwritten with "Strawberry"
  cakeFlavours[4] = "Lemon";
  //inserting requires all values to be
  //moved up one index.  If inserting at 0th index - O(n):

  //OVERWRITE visualization:
  cout << "static array before insert and overwrite:" << endl;
  for(int i = 0; i < 5; i++) {
    cout << cakeFlavours[i] << endl;
  }
  cout << "-------------------" << endl;
  //...and an insert:
  for(int i = 5; i > 2; i--) {
    cakeFlavours[i] = cakeFlavours[i - 1];
  }
  cakeFlavours[2] = "Raspberry";
  //this is an 'overwrite':
  cakeFlavours[3] = "Strawberry";
  //INSERT visualization:
  cout << "static array after insert and overwrite:" << endl;
  for(int i = 0; i < 6; i++) {
    cout << cakeFlavours[i] << endl;
  }
  cout << "-------------------" << endl;
  //...and a delete:
  for(int i = 2; i < 5; i++) {
    cakeFlavours[i] = cakeFlavours[i + 1];
  }
  //DELETE visualization:
  cout << "static array after delete:" << endl;
  resize(cakeFlavours, 5);
  for(int i = 0; i < 5; i++) {
    cout << cakeFlavours[i].c_str() << endl;
  }
  cout << "-------------------" << endl;

  // gasPrices[0] = 340;
  // gasPrices[1] = 204;
  // gasPrices[2] = 289;
  // gasPrices[3] = 315;
  // gasPrices[4] = 329;

  // cout << "struct array of integers with 5 elements:" << endl;
  // for(int i = 0; i < gasPrices.size(); i++) {
  //   cout << gasPrices[i] << endl;
  // }
  // cout << "-------------------" << endl;

  // sequence[0] = 111;
  // sequence[1] = 111;
  // sequence[2] = 111;
  // sequence[3] = 111;
  // sequence[4] = 111;

  // sequence[3] = 101;

  // cout << "static array of integers with 6 elements (init with 0):" << endl;
  // for(int i = 0; i < 5; i++) {
  //   cout << sequence[i] << endl;
  // }
  // cout << "-------------------" << endl;

  return 0;
}


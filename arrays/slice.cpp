#include <iostream>
#include <vector>
#include <algorithm>
#include <ncurses.h>
#include <unistd.h>
using namespace std;

#define DELAY 40000

void editElements(vector<int>::iterator itr1, vector<int>::iterator itr2) {
  for (; itr1 != itr2; itr1++) {
    *itr1 += 3;
  }
}

int main() {
  vector<int> v{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  editElements(v.begin() + v.size() / 2, v.end());
  newterm(NULL, stdin, stdout);
  noecho();

  for (vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
    it == v.begin() ? printw("%s", to_string(*it).c_str()) : printw("\t%s", to_string(*it).c_str());
    it == v.end() - 1 ? printw("%s","\n") : printw(""); 
  } 
  printw("%s","\n");
  
  int startIndex = v.size() / 2 * 8;
  move(1, startIndex);
  int oddNumberFactor = 0;
  v.size() % 2 == 1 ? oddNumberFactor = -1 : oddNumberFactor = 7;
  for (int i = 0; i < startIndex - oddNumberFactor; ++i) {
    mvaddch(1, i + startIndex, ACS_CKBOARD);
    i % 2 == 0 ? usleep(DELAY) : usleep(0);
    refresh();
  }
  getch(); 
  endwin();
}
